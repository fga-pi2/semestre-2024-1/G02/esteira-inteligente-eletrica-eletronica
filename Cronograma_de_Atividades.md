# Cronograma de Atividades
## Entregas formais da disciplina

### PC1 24/04
#### PC1 - Definição de componentes   Data: 24/04 <b>(final) </b>


Nesta data será entregue a lista definida de componentes que serão utilizados Na parte de
Eletrônica/Energia
  
#### PC1 - Esquemáticos / Diagramas   Data: 01/05 <b>(final) </b>


Nesta data serão entregues os esquemáticos e diagramas dos componentes
utilizados em Eletrônica/Energia

Status: <b> Parcilamente definido </b>


### PC2 - Validação e testes dos esquemáricos  Data: 28/05 <b>(final) </b>



Nesta data serão entregues os resultados preliminares de implementação e montagem dos Subsistemas
de Eletrônica/Energia.

Status: <b> Não Iniciado </b>


### PC3 - Sistema funcional  Data: 19/06 <b>(final) </b>

Nesta estapa ocorrerá a integração completa dos subsistemas de Eletrônica/Energia.

## Entregas Informais

<b>EM DEFINIÇÃO </b>